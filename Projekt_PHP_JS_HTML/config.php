<?php

$defaultParams = array(
    'windowWidth' => 400,
    'windowHeight' => 200,
    'borderWeight' => '1px',
    'borderColor' => '#000',
    'windowColor' => '#fff',
    'backgroundColor' => '#fff',
    'fontFamily' => "'monospace'",
    'fontWeight' => 100,
    'fontSize' => '12px',
    'animationTempo' => 2000,
    'animationFreeze' => 1000,
    'animationDirection' => 'down'
);

$defaultLinks = array(
    0 => array('label' => 'WirtualnaPolska',
        'url' => 'www.wp.pl'),
    1 => array('label' => 'Onet',
        'url' => 'www.onet.pl'),
    2 => array('label' => 'Interia',
        'url' => 'www.interia.pl')
);
