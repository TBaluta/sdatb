<?php
include 'html/head.html';
include 'html/header.php';


//        . 'a=anim'
//        . '&windowWidth='.$_POST['windowWidth']
//        . '&windowHeight='.$_POST['windowHeight']
//        . '&borderWeight='.$_POST['borderWeight']
//        . '&borderColor='.$_POST['borderColor']
//        . '&windowColor='.$_POST['windowColor']
//        . '&backgroundColor='.$_POST['backgroundColor']
//        . '&fontFamily='.$_POST['fontFamily']
//        . '&fontWeight='.$_POST['fontWeight']
//        . '&fontSize='.$_POST['fontSize']
//        . '&animationTempo='.$_POST['animationTempo']
//        . '&animationFreeze='.$_POST['animationFreeze']
//        . '&animationDirection='.$_POST['animationDirection']
//        
//    $defaultParams=array(
//    'windowWidth'=>400,
//    'windowHeight'=>200,
//    'borderWeight'=>'1px',
//    'borderColor'=>'#000',
//    'windowColor'=>'#fff',
//    'backgroundColor'=>'#fff',
//    'fontFamily'=>"'monospace'",
//    'fontWeight'=>100,
//    'fontSize'=>'12px',
//    'animationTempo'=>2000,
//    'animationFreeze'=>1000,
//    'animationDirection'=>'down'   


echo $link;
?>

<!--##########################################################################################-->

<form class='websites' method='POST' action='form.php'>
    <h2>Dodawanie stron do wyświetlenia</h2>
    <div class='row'>
        <label for='urlAddress'>Podaj adres strony: </label>
        <input type='text' name='urlAddress' id='urlAddress' value='' placeholder='adres URL'/>
    </div>         

    <div class='row'>
        <label for='label'>Podaj etykietę strony: </label>
        <input type='text' name='label' id='label' value='' placeholder='etykieta'/>
    </div> 

    <div class='row'>
        <input class='button' type='submit' value='Dodaj do bazy'/>
    </div>
</form>

<!--##########################################################################################-->  

<!--          Rozmiar okna             -->

<form action='form.php' class='tableAttributes' method='POST'>

    <h2>Atrybuty okna</h2>

    <div class='row'>
        <label for='windowWidth'>Podaj szerokość okna w pixelach: </label>
        <input type='text' name='windowWidth' id='windowWidth' value='' placeholder='szerokosc okna'/>
    </div> 

    <div class='row'>
        <label for='windowHeight'>Podaj wysokość okna w pixelach: </label>
        <input type='text' name='windowHeight' id='windowHeight' value='' placeholder='wysokość okna'/>
    </div> 

    <!--          Obramowanie             -->

    <div class='row'>
        <label for='borderWeight'>Wybierz grubość obramowania: </label>
        <select name='borderWeight' id='borderWeight'>
            <option value='1px'>1px</option>
            <option value='2px'>2px</option>
            <option value='3px'>3px</option>
            <option value='5px'>5px</option>
            <option value='8px'>8px</option>
            <option value='10px'>10px</option>
        </select>
    </div> 

    <div class='row'>
        <label for='borderColor'>Wybierz kolor obramowania: </label>
        <input id='borderColor' name='borderColor' value="fff" class="jscolor {width:101, padding:0, shadow:false, borderWidth:0, backgroundColor:'transparent', insetColor:'#000'}">
    </div>

    <!--          Wnętrze okna             -->

    <div class='row'>
        <label for='windowColor'>Wybierz kolor okna: </label>
        <input id='windowColor' name='windowColor' value="fff" class="jscolor {width:101, padding:0, shadow:false, borderWidth:0, backgroundColor:'transparent', insetColor:'#000'}">
    </div>

    <div class='row'>
        <label for='backgroundColor'>Wybierz kolor tła: </label>
        <input id='backgroundColor' name='backgroundColor' value="fff" class="jscolor {width:101, padding:0, shadow:false, borderWidth:0, backgroundColor:'transparent', insetColor:'#000'}">
    </div>

    <div class='row'>
        <label for='fontFamily'>Wybierz rodzaj czcionki: </label>
        <select name='fontFamily' id='fontFamily'>
            <option value='Sans-serif'>bezszeryfowa</option>
            <option value='Serif'>szeryfowa</option>
            <option value='Monospace'>monospace</option>
        </select>
    </div> 

    <div class='row'>
        <label for='fontStyle'>Wybierz styl czcionki: </label>
        <select name='fontStyle' id='fontStyle'>
            <option value='normal'>normalny</option>
            <option value='italic'>kursywa</option>
        </select>
    </div> 

    <div class='row'>
        <label for='fontWeight'>Wybierz grubość czcionki: </label>
        <select name='fontWeight' id='fontWeight'>
            <option value='100' style='font-weight:100;'>100</option>
            <option value='200' style='font-weight:200;'>200</option>
            <option value='300' style='font-weight:300;'>300</option>
            <option value='400' style='font-weight:400;'>400</option>
            <option value='500' style='font-weight:500;'>500</option>
            <option value='600' style='font-weight:600;'>600</option>
            <option value='700' style='font-weight:700;'>700</option>
            <option value='800' style='font-weight:800;'>800</option>
            <option value='900' style='font-weight:900;'>900</option>
        </select>   
    </div> 

    <div class='row'>
        <label for='fontSize'>Wybierz wielkość czcionki: </label>
        <select name='fontSize' id='fontSize'>
            <option value='12px' style='font-size:12px;'>normalna 12px</option>
            <option value='18px' style='font-size:18px;'>średnia 18px</option>
            <option value='24px' style='font-size:22px;'>duża 22px</option>
            <option value='28px' style='font-size:28px;'>bardzo duża 28px</option>
        </select>
    </div> 

    <div class='row'>
        <label for='fontColor'>Wybierz kolor czcionki: </label>
        <input id='fontColor' name='fontColor' value="fff" class="jscolor {width:101, padding:0, shadow:false, borderWidth:0, backgroundColor:'transparent', insetColor:'#000'}">
    </div>

    <div class='row'>
        <input class='button' type='submit' value='Dodaj do bazy'/>
    </div>

</form>

<?php
include 'html/footer.php';
