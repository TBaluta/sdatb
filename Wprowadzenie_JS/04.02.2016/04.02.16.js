document.getElementById('get').onclick = function () {
    var x = document.getElementById('jeden').value;
    x = parseInt(x);
    console.log(x);
    var y = document.getElementById('dwa').value;
    y = parseInt(y);
    console.log(y);
    return NWD(x, y);
};

function NWD(x, y) {
    if (isNaN(x) == true || isNaN(y) == true || x == 0 || y == 0) {
        alert('Podałeś wartości nieliczbowe lub 0 trolu!');
        return false;
    }

    var text = 'NWD liczby ' + x + ' oraz liczby ' + y + ' wynosi:';

    if (x < 0) {
        x *= -1;
    }

    if (y < 0) {
        y *= -1;
    }

    while (x !== y) {
        if (x > y) {
            x = x - y;
        } else if (y > x) {
            y = y - x;
        }
    }

    text += x;
    document.getElementById('result').style.display = 'block';
    document.getElementById('result').innerHTML = text;
    return true;
}

document.getElementById('get2').onclick = function () {
    var a = document.getElementById('data').value;
    a = parseInt(a);
    console.log(a);
    return sumaCyfr(a);
};

function sumaCyfr(a) {

    var cyfra = 0;
    var text = 'Suma cyfr danej liczby wynosi ';

    if (isNaN(a) === true) {
        alert('Podałeś wartości nieliczbowe trolu!');
        return false;
    }

    if (a < 0) {
        a *= -1;
    }

    if (a < 10) {
        cyfra = a;
    } else {
        while (a > 0) {
            cyfra = cyfra + a % 10;
            a = Math.floor(a / 10);
        }
    }

    text += cyfra;
    document.getElementById('result2').style.display = 'block';
    document.getElementById('result2').innerHTML = text;
    return cyfra;
}

//var tablica = [17, 49, 40, 40];
//var tablica2 = ['abcdr', 123, true, tablica, sumaCyfr(32424)];
//console.log(tablica, tablica2);
//
//console.log(tablica[2]);
//var tab = new Array(); //deklarowanie nowej, ale pustej tablicy
//
//var str = 'Jakiś napis ze spacjami';
//console.log(str);
//
//str = str.split('a');
//console.log(str);

document.getElementById('gett').onclick = function () {
    var a = document.getElementById('eps').value;
    a = parseInt(a);
    return sumaCyfr(a);
};

function sumaCyfr(a) {

    var liczba = 0;

    var text = 'Suma cyfr danej liczby wynosi ';

    var aJakoString = a.toString();
    console.log(a, aJakoString);

    var aJakoArray = aJakoString.split('');
    console.log(aJakoArray);

    var iloscCyfr = aJakoArray.length;

    for (i = 0; i < aJakoArray.length; i++) {
        var num = parseInt(aJakoArray[i]);

        liczba += num;
        console.log(liczba);
    }

    text += liczba;
    document.getElementById('result3').style.display = 'block';
    document.getElementById('result3').innerHTML = text;
    return liczba;

};

