$(document).ready(function(){ // $ oznacza ze jest to dokument jquery. ready 
    //oznacza ze funkcja wykona sie gdy strona zaladuje sie w calosci
    
    $('#button').click(function(){ //po kliknieciu w element o nazwie button odpal funkcje
        if ($(this).hasClass('active')) {
            $(this).text('Kliknij').removeClass('active');
        }
        else {
            $(this).text('Kliknięto').addClass('active'); 
        }
        $('.switch').each(function() {
            if($('#button').hasClass('active')) {
                if($(this).hasClass('right')) {
                   $(this).animate({'left' : 100}, 1000); 
                }
                else {
                   $(this).animate({'right' : 100}, 1000);
                }
            }
            else {
                if($(this).hasClass('right')) {
                   $(this).animate({'left' : 700}, 1000);
                }
                else {
                   $(this).animate({'right' : 700}, 1000); 
                }
            }
//            $(this).fadeOut(500).delay(500).queue(function(){
//                $(this).show();
//            }
        });
    });
});
