document.getElementById('get').onclick = function () {
    var n = document.getElementById('pow').value;
    n = parseInt(n);
    // alert('Silnia liczby ' + n + ' to ' + silnia(n));
    var text = 'Silnia liczby ' + n + ' to ' + silnia(n);
    document.getElementById('result1').style.display = 'block';
    document.getElementById('result1').innerHTML = text;
};

function silnia(n) {

    if (isNaN(n) === true || n < 0) {
        alert('podałeś wartości nieliczbowe lub ujemne');
        return false;
    }
    if (n == 0) {
        return 1;
    } else {
        return n * silnia(n - 1); // najprostszy przykład rekurencji czyli wywołyawnia funkcji przez samą siebie
    }
}

document.getElementById('get2').onclick = function () {
    var array = document.getElementById('sort').value;
    return sortArray(array);

};

function sortArray(array) {

    var tablica = array.split(',');
    var comunicate = 'Nieposortowana tablica';
    console.log(tablica);

    for (var i = 0; i < tablica.length; i++) {
        tablica[i] = parseInt(tablica[i].trim());
        comunicate += tablica[i] + ',';
        // trim wycina spacje 
        //(domyślnie - może to być dowolny znak lub ciąg znaków) z przed kazdego 
        //elementu i po nim (jesli element ma typ string)
    }

    for (var j = 0; j < tablica.length - 1; j++) {
        console.log(tablica[j]);
        for (var k = 0; k <= tablica.length - 1; k++) { // <= ponieważ ostatni element nie ma pary i 
            // nie ma go z czym porównywać
            var elem = tablica[k];
            var next = tablica[k + 1];
            console.log(elem, next);
            if (elem > next) {      // zamiana miejscami w zależności co jest większe, a co mniejsze(porównaie)
                tablica[k] = next;  // jesli elem > next następuje zamiana miejsc
                tablica[k + 1] = elem;
            }
        }
    }
    // sortowanie babelkowe polega na tym, że następuje porownanie elenmentów w parach
    // 37, 736, 234, 23, 32, 11
    console.log(tablica);
}

