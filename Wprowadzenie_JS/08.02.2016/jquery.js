$(document).ready(function () { // $ oznacza ze jest to dokument jquery. ready 
    //oznacza ze funkcja wykona sie gdy strona zaladuje sie w calosci

    $('#button').click(function () { //po kliknieciu w element o nazwie button odpal funkcje
        if ($(this).hasClass('active')) {
            $(this).text('Kliknij').removeClass('active');
        } else {
            $(this).text('Kliknięto').addClass('active');
        }
        $('.switch').each(function () {
            if ($('#button').hasClass('active')) {
                if ($(this).hasClass('right')) {
                    $(this).animate({'left': 100}, 1000);
                } else {
                    $(this).animate({'right': 100}, 1000);
                }
            } else {
                if ($(this).hasClass('right')) {
                    $(this).animate({'left': 700}, 1000);
                } else {
                    $(this).animate({'right': 700}, 1000);
                }
            }
//            $(this).fadeOut(500).delay(500).queue(function(){
//                $(this).show();
//            }
        });
    });
});



$(document).ready(function () { // ready sprawia ze kod zostanie zaladowany 
    //dopiero gdy caly kod zostanie wykonany. Ready uzyta na obiekcie dokument
    // ze znakiem $ oznacza uzywanie jquery
    $('form').submit(function (e) { // submit - wysłanie formularz po którym natstepuje
        //przeładowanie strony
        e.preventDefault(); // blokuje domyślne działanie submita w tym wypadku
        // czyli domyślne wysyłanie
        var tab = {};
        var message = '';
        var error = 0;

        $(this).children().find('input').each(function () { // odwołanie do wszystkich
            //inputów w html plus petla iteracyjna each. This'em jest zawsze najwyzsza 
            //instancja dla przegladarki. W tym wypadku form. Each pobiera całe inputy
            // jako obiekty

            $(this).removeClass('error valid');

            var name = $(this).attr('name');
            var type = $(this).attr('type');
            var placeholder = $(this).attr('placeholder');
            var value = $(this).val();
//            tab[$(this).attr('name')] = $(this);

            if (type !== 'submit') {

                if (type === 'text' && (value.length < 3 || value.length > 20)) {
                    $(this).addClass('error');
                    message += 'Wypełnij pole ' + placeholder + '\n';
                    error = 1;
                } else if (type === 'number') {
                    if (name === 'phone' && value.length < 9) {
                        $(this).addClass('error');
                        message += 'Wypełnij pole ' + placeholder + '\n';
                        error = 1;
                    } else {
                        $(this).addClass('valid');
                    }
                }

//                if (value =='') {
//                    $(this).addClass('error');
//                }
                else {
                    $(this).addClass('valid');
                }         
                console.log(name, type, placeholder, value);
            }
        });

        if (error === 1) {
            alert(message);
        }
        
        console.log(tab);
    });
});

$(window).load(function () { // load będzie wczytywał kod w czasie wczytywania strony,
    // a nie jak w ready - dopiero po załadowaniu

});


/////////////////////////////////////////////////////////////////////////////////////////////////

var gryphon1 = [12, 8481, 5, 1, 515, 51, 8, 4, 51, 8, 7, 48, 84, 81, 51, 88, 2, 0, 1, 158];
var gryphon2 = [1, 34, 4, 6, 8, 9, 54, 67, 87, 23, 12, 34, 2, 13, 45, 67, 56];
var even = [];

function segregacja(a,b) {
    return a-b;
}

function sameParzyste(array)
{
    var j = 0;
    
    for (i = 0; i < array.length; i++) {
        if (array[i] % 2 === 0) {
            even[j] = array[i];
            j += 1;
        }
    }
    return even;
}
sameParzyste(gryphon1);
console.log(even.sort(segregacja).reverse());


