document.getElementById('get').onclick = function () {
    var x = document.getElementById('data').value; // pobieramy wartość pierwszego pola takstowego
    x = parseInt(x); // zamiana typu tekstowego, który przybrała zmienna x na typ number
    console.log(x);
    var a = document.getElementById('data2').value;
    a = parseInt(a);
    console.log(typeof (a));
    return square(x, a);
};


function square(x, a) {

    var result = 1; // zmienna pomocnicza wprowadzona w celu uniknięcia 
    //pomnożenia na początku pętli x*x (powinno być x*1) Na przykład x=4, a=5 
    // x*x=16, zamiast x*1=4

    if (isNaN(x) == true || isNaN(a) == true) {
        alert('podałeś wartości nieliczbowe');
        return false; // return kończy działanie f-cji, jeśli false --> 
        //koniec, nie idzie dalej
    }
    if (a < 0) {
        var check = 1;
        a *= -1; // a = a * (-1)
    }

    for (var i = 1; i <= a; i++) {
        result *= x;
    }

    if (check) {
        result = 1 / result;
    }

    var text = 'Wynik potęgowania liczby ' + x + ' do potęgi ' + a + ' to ' + result;
    document.getElementById('result').style.display = 'block'; // zmieniamy 
    //style.display none ( w spanie w html) na style.display block żeby się wyświetlał
    document.getElementById('result').innerHTML = text; // wnikamy wewnątrz HTML'a 
    //określonej przez id
    console.log(result);
    return true;
}

// javascript Math. --> funkcje matematyczne