var Plugin = function(){
    var wypisz_1 = function() {
        alert('1');
    };

    wypisz_3 = function () {   // trzeba pisać "var" bo na starszych przeglądarkach może się sypać
        alert('3');
    };
    
    function wypisz_4(){
        alert('4');   
    };
    
    return {
        'wypisz_1': wypisz_1,
        //'wypisz_2': wypisz_2,
        'wypisz_3': wypisz_3,
        'wypisz_4': wypisz_4
    };
};

var pl = new Plugin();

//////////////////////////////////////////////////////////////////////////////////////////////////

var Parzyste = function(start, end) {
    var nastepny = start<end?( function(i) {  return i+1;}) : (function(i) {return i-1;});
    
    while(start !== end) {
        //if (start%2 === 0){
        console.log(start);
        start = nastepny(start);
        
    }
    console.log(start);
};

///////////////////////////////////////////////////////////////////////////////////////////////////

var sekundy = function(time){
    //hh:mm:ss;
    //debugger;
    var sek = time / 60 && time % 60;
    var mm = (time - sek) / 60;
    var min = mm % 60;
    var hour = mm / 60;
    hour = parseInt(hour, 10);
    
    if ( sek < 10){sek = '0' + sek;} 

    var wypisz = function(cos){
        if(cos === 0){ return cos = ''; }
        if(cos < 10){ return cos = '0' + cos + ':';}
        else  {return cos + ':';}
        
    };
    text = wypisz(hour) + wypisz(min) + sek;
    
    console.log(text);
};

sekundy(1565);


var slowa = ['a', 'aa', 'b', 'c', 'a', 'c'];

var zliczone = {};

slowa.forEach(function (el) {
    if (!zliczone.hasOwnProperty(el)) {
        zliczone[el] = 0;
    }
    zliczone[el]++;
});
console.log(zliczone);

////////////////////////////////////////////////////////////

var slowa = ['a', 'aa', 'b', 'c', 'a', 'c'];

var zliczone = {};

slowa.forEach(function (el) {
    zliczone[el] = (zliczone.hasOwnProperty(el)
            ? zliczone[el]
            : 0) + 1;
});
console.log(zliczone);

/////////////////////////////////////////////////////////////

var slowa = ['a', 'aa', 'b', 'c', 'a', 'c'];
var zliczone = {};
slowa.forEach(function (el) {
    
    try {
        zliczone[el] += 1;
    } catch (err) {
        zliczone[el] = 1;
    }
});
console.log(zliczone);

////////////////////////////////////////////////////////////////

var slowa = ['a', 'aa', 'b', 'c', 'a', 'c', 'c', 'd'];
var zliczone = {};
slowa.forEach(function (el) {

    zliczone[el] = (zliczone[el] || 0) + 1;
});
console.log(zliczone);

var data = new Date(1984, 04, 22);
console.log(data.getFullYear());
console.log(data.getMonth());
console.log(data.getDate());

var dzis = new Date();
console.log(dzis.toDateString());

console.log(dzis-data);
console.log((dzis-data)/(1000*60*60*24*365));

////////////////////////////////////////////////////////////////////

function age()
{
    var today = new Date();
    var date = new Date(2015, 04, 24);
    var wiek = 0;

    var dateYear = date.getFullYear();
    var dateMonth = date.getMonth();
    var dateDay = date.getDate();
    console.log(dateYear, dateMonth, dateDay);

    var todayYear = today.getFullYear();
    var todayMonth = today.getMonth();
    todayMonth += 1;
    var todayDay = today.getDate();
    console.log(todayYear, todayMonth, todayDay);

    if (dateYear > todayYear) {
        alert('Data musi być wcześniejsza od dzisiejszej');
        return false;
    }

    if ((todayYear > dateYear && todayMonth < dateMonth) || (todayYear > dateYear && todayMonth === dateMonth && dateDay <= todayDay)) {
        wiek = todayYear - dateYear;
    } else {
        wiek = todayYear - (dateYear + 1);
    }
    console.log(wiek);
}
console.log(age());

///////////////////////////////////////////////////////////////////////

function gen_kody(begin, end) {

    var begin = '02-200';
    var end = '02-302';
    
    var beginFirstTwo = begin.slice(0,2);
    var beginLastThree = begin.slice(3,6);
    var beginAsString = beginFirstTwo.concat(beginLastThree);
    var beginAsInt = parseInt(beginAsString);
    console.log(beginAsInt);
    
    var endFirstTwo = end.slice(0,2);
    var endLastThree = end.slice(3,6);
    var endAsString = endFirstTwo.concat(endLastThree);
    var endAsInt = parseInt(endAsString);
    console.log(endAsInt);
    
    for (var i=beginAsInt; i<=endAsInt; i++) {
        console.info(i);
    }
    
//    var beginToObject = begin.split('-');
//
//    for (var i = 0; i < beginToObject.length; i++) {
//        var beginLiczby = parseInt(beginToObject[i]);
//        console.log(beginLiczby);
//    }
//
//    var endToObject = end.split('-');
//
//    for (var i = 0; i < endToObject.length; i++) {
//        var endLiczby = parseInt(endToObject[i]);
//        console.log(endLiczby);
//    }

}
console.log(gen_kody());



// INPUT gen_kody('02-200','02-302')

//console.log(parseInt('02200'));

// OUTPUT 80-200, 80-201, 80-202