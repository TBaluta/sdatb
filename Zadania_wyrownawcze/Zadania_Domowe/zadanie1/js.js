var zadania = [
    {tresc: "wyprowadzić psa", wykonane: true},
    {tresc: "zrobić zakupy", wykonane: false},
    {tresc: "nauczyć się programować", wykonane: false}
];

function listaZadan() {
//sortowanie obiektu
    zadania.sort(function (a, b) {
        return a.wykonane - b.wykonane;
    });

    var j = 0;
    var t = 0;
    var text = '<ol>';
    for (i = 0; i < zadania.length; i++) {
        var k = i + 1;
        if (zadania[i].wykonane === true) {
            j += 1;
            text += '<li style="color:grey" value="' + k + '">' + zadania[i].tresc + '<input class="checkbox" type="checkbox" checked="checked" value="true"/></li>';
        } else {
            t += 1;
            text += '<li value="' + k + '">' + zadania[i].tresc + '<input class="checkbox" type="checkbox" value="false"/></li>';
        }

    }
    

    text = '<span> Liczba zadań wykonanych: ' + j + '. Liczba zadań niewykonanych: ' + t + '. </span>' + text + '</ol>';
    document.getElementById('cont').innerHTML = text;


}

window.onload = listaZadan;

$('#cont').on('click', '.checkbox', function () {

    var spr = $(this).val();
    var wartosc = $(this).parents('li').val();

    if (spr === 'true') {
        zadania[wartosc - 1].wykonane = false;
    } else {
        zadania[wartosc - 1].wykonane = true;
    }

    listaZadan();
});