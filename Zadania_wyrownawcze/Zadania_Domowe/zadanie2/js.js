$('#faqList').on('click', '.question', function(e){

  if($(this).next('dd').is(':visible')){
    return false;
  }

 $('#faqList').find('dd:visible').stop(true,true).slideUp();

 $(this).next('dd').stop(true,true).slideDown();

});

