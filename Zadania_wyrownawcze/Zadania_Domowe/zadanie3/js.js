var guziki = '<button class="previous" type="button">Poprzedni</button><button class="next" type="button">Następny</button>';
$('#slideshow').append($(guziki));
var choosen = 1;
var elements = $('#slideshow > div');

function slide(how) {
    for (i = 0; i < elements.length; i++) {

        if (i > choosen || i < choosen - 1) {
            $(elements[i]).hide(how);
        } else if (i === choosen || i === choosen - 1) {
            $(elements[i]).show(how);
        }
    }
}

slide();

$('#slideshow').on('click', '.next', function () {
    if (choosen === elements.length - 1) {
        alert('ostatni obrazek');
    } else {
        choosen += 2;
    }
    slide("slow");
});

$('#slideshow').on('click', '.previous', function () {
    if (choosen === 1) {
        alert('pierwsza obrazek');
    } else {
        choosen -= 2;
    }
    slide("slow");
}); 