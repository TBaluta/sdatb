(function () {

    var player = document.getElementById('audio');

    playlist = [
        {
            title: '"Great Balls of Fire", Blackwell & Hammer',
            source: 'http://www.stephaniequinn.com/Music/Commercial%20DEMO%20-%2013.mp3'
        },
        {
            title: '"Jailhouse Rock", Leiber & Stoller',
            source: 'http://www.stephaniequinn.com/Music/Commercial%20DEMO%20-%2010.mp3'
        },
        {
            title: '"My Girl" (Motown) , Temptations',
            source: 'http://www.stephaniequinn.com/Music/Commercial%20DEMO%20-%2009.mp3'
        }
    ];


    var text = '<ol>';
    for (i = 0; i < playlist.length; i++) {
        text += '<li value="' + (i + 1) + '">' + playlist[i].title + '</li>';

    }
    text += '</ol> <label for="checkbox">Automatyczne odtwarzanie następnego utworu</label><input type="checkbox" value="true" id="checkbox"> </input>';
    document.getElementById('list').innerHTML = text;

    var currentSong = 0;

    $('#list').on('click', 'li', function () {
        currentSong = $(this).val() - 1;
        player.src = playlist[currentSong].source;
        player.play();

    });
    $('#list').on('click', '#checkbox', function () {
        
        if (this.checked) {
            player.onended = function ()  {

                if (currentSong === playlist.length - 1) {
                    currentSong = 0;
                } else {
                    currentSong += 1;
                }
                player.src = playlist[currentSong].source;
                player.play();
            };

           
        } else {
            
            player.onended = function (){
            };
            return;
        }
    });

    player.src = playlist[currentSong].source;
    player.play();

// metody
// player.src = 'http://link_do_mp3';
// player.load() -> wczytuje utwór
// player.play() -> rozpoczyna odtwarzanie

// eventy (https://developer.mozilla.org/pl/docs/Web/API/Element/addEventListener)
// ended -> utwór doszedł do końca

})();

