<?php

session_start();
error_reporting(0);
header('Content-Type: text/html; charset=UTF-8');

if (isset($_GET['odNowa']) && $_GET['odNowa'] == 'true') {
   $_SESSION['PLAYA']['quest']=1; // USTAWIENIE POZIOMU NA 1
   unset($_SESSION['nastepnePytanie']);
   header("HTTP/1.1 301 Moved Permanently");
   header('location: index.php');
}


if (isset($_GET['logout']) && $_GET['logout'] == 'true') {
    unset($_SESSION['PLAYA']); // USUNIĘCIE INFORMACJI O LOGOWANIU
    unset($_SESSION['nastepnePytanie']);
    header("HTTP/1.1 301 Moved Permanently");
    header('location: index.php');
}


/* CZY GRA ZOSTAŁA ROZPOCZĘTA */
if (isset($_SESSION['PLAYA']) && $_SESSION['PLAYA'] != NULL) { // CZY ISTNIEJE TABLICA "$_SESSION['PLAYA']"

    /* GRA ZOSTAŁA ROZPOCZĘTA */

    echo 'Gracz: ' . $_SESSION['PLAYA']['player'];
    echo $logout = '<a href="index.php?logout=true"><button style="float:right;">Wyloguj</button></a>';
    echo $odNowa = '<a href="index.php?odNowa=true"><button style="float:right;">Zacznij od nowa!</button></a>'.'<br/><br/><br/>';

    $file = fopen('pytania.txt', 'r'); // OTWARCIE PLIKU Z PYTANIAMI
    $questions = fread($file, filesize('pytania.txt'));

    require_once 'questions.php';
}


else { // GRA NIE ZOSTAŁA ROZPOCZĘTA

    /* CZY DODALIŚMY NAZWĘ UŻYTKOWNIKA */
    if (isset($_POST['user']) && $_POST['user'] != NULL) { // CZY ISTNIEJE TABLICA $_POST A W NIEJ ELEMENT O KLUCZU "user"
        /* TAK */
        $_SESSION['PLAYA']['player'] = $_POST['user']; // WPISANIE NAZWY GRACZA DO SESJI
        $_SESSION['PLAYA']['quest'] = 1; // USTALENIE NUMERU PYTANIA NA 1
        header("HTTP/1.1 301 Moved Permanently"); // 
        header('location: index.php'); // PRZEŁADOWANIE STRONY
    } else { /* NIE */
        require_once 'login.php'; // WYŚWIETLENIE FORMULARZA DODANIA UŻYTKOWNIKA
    }
}