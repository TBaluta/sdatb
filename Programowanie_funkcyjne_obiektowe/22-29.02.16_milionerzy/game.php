<?php
$poziom = $_SESSION['PLAYA']['quest'];

function zapiszUzytkownikaDoTopRekordow($poziom) {
    $username = $_SESSION['PLAYA']['player'];
    
    if (!isset($_SESSION['TOP_SCORE_NEW'])) {
        $_SESSION['TOP_SCORE_NEW'] = array(); 
        // od php 5.4 mozna uzyc $zmienna = [];
    }
    
    $topScore = $_SESSION['TOP_SCORE_NEW'];
    
    // "gorszy" wynik Andrzeja nie powinien nadpisywac
    // "lepszego" wyniku innego Andrzeja

    if (!isset($topScore[$username])) { // jeśli wyniku nie ma - wpisz dowolny
        $topScore[$username] = $poziom;
    } else {
        // Jeśli wynik jest - nadpisz tylko, jeśli nowy jest większy od startowego
        $staryWynik = $topScore[$username];
        
        if ($poziom > $staryWynik) {
            $topScore[$username] = $poziom;
        }
    }
    
    natsort($topScore);
    $topScore = array_reverse($topScore);
    $topScore = array_slice($topScore, 0, 5);
    
    $_SESSION['TOP_SCORE_NEW'] = $topScore;
}


// Funkcja użyta w celu uniknięcia duplikowania tego samego kodu w kilku miejscach
function wyswietlTopUzytkownikow() {
    // Sesja pelni role bazy - jesli jej nie ma - tworzymy pusta baze
    if (!isset($_SESSION['TOP_SCORE_NEW'])) {
        $_SESSION['TOP_SCORE_NEW'] = array(); 
        // od php 5.4 mozna uzyc $zmienna = [];
    }

    $topScore = $_SESSION['TOP_SCORE_NEW'];
    
    foreach ($topScore as $user => $value) {
        echo ($user . ' - level: ' . $value . '<br>');
    }
}


if (isset($_POST['odpowiedz']) && $_POST['odpowiedz'] != NULL) {
    $odp = $_POST['odpowiedz'];
    $pyt = $_POST['question'];
    
    $poprawna = $wszystkiepytania[$poziom][$pyt][4];
    $poprawna = rtrim($poprawna);  // trim wycina wszystkie znaki białe z początku i końca stringa
    
    unset($_SESSION['nastepnePytanie']);
    if ($poprawna == $odp) {
        
        echo "Brawo! Odpowiedź poprawna!</br></br>";
        
        if ($poziom == count($wszystkiepytania)) { // Count zwraca liczbe elementow w tablicy
            // zapisuje uzytkownika do topscore wraz z levelem konczacym gre
            zapiszUzytkownikaDoTopRekordow(count($wszystkiepytania));
            wyswietlTopUzytkownikow();
            echo 'Gratulacje!';
            $_SESSION['PLAYA']['quest'] = 1;
        }
        
        else {
            $poziom += 1; 
            $_SESSION['PLAYA']['quest'] +=1;
            
                // problem: odswiezenie strony generuje nowe pytanie
                // (1) jezeli mamy w sesji pytanie
                // (2) to je wyswietl
                // (3) jezeli nie mamy 
                // (4) wygeneruj, zapisz do sesji i wyswietl
            
            if (isset($_SESSION['nastepnePytanie'])) { // (1)
                $pytanie = $_SESSION['nastepnePytanie']; // (2)
            } else { // (3)
                $pytanie = array_rand($wszystkiepytania[$poziom], 1); // (4)
                $_SESSION['nastepnePytanie'] = $pytanie; // (4)
            }

            require_once 'forma.php';
        }
        
    }
    else {
        // zapisuje uzytkownika do topscore wraz z aktualnym levelem
        zapiszUzytkownikaDoTopRekordow($_SESSION['PLAYA']['quest']);
        wyswietlTopUzytkownikow();
        $_SESSION['PLAYA']['quest']=1; // USTAWIENIE POZIOMU NA 1
        echo 'Odpowiedź błędna. Koniec Gry!</br></br>';
    }
    
} 

else if ($_POST['odpowiedz'] == NULL && $_POST['question'] != NULL) { 
    $pytanie = $_POST['question'];
    require_once 'forma.php';
}

else {
    if (isset($_SESSION['nastepnePytanie'])) {
        $pytanie = $_SESSION['nastepnePytanie'];
    } else {
        $pytanie = array_rand($wszystkiepytania[$poziom], 1);
        $_SESSION['nastepnePytanie'] = $pytanie;
    }
    
    require_once 'forma.php';
}




