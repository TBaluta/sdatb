function Samochod(c, m, t, y, e) {
    this.marka = c;
    this.model = m;
    this.typ = t;
    this.rok = y;
    this.silnik = e;
    this.wpis = function() {
       var wierszTabeli = '<tr>';
       wierszTabeli += '<td>' + this.marka + '</td>';
       wierszTabeli += '<td>' + this.model + '</td>';
       wierszTabeli += '<td>' + this.typ + '</td>';
       wierszTabeli += '<td>' + this.rok + '</td>';
       wierszTabeli += '<td>' + this.silnik + '</td></tr>';
       
       return wierszTabeli;
    };
}

$(document).ready(function(){ // callback dla funkcji ready, ktory to jest funkcją anonimową (ten callback)
    $('#button').click(function(e) {
        e.preventDefault(); // dzieki tej funkcji formularz sie nie wysyla
//        console.log(e);
        
        var klop = {};
        
        $('input[type="text"]').each(function() { // pobranie wszystkich wartosci tekstowych z input
            $(this).val(); // zmienna wartosc przybiera wartosc tekstową tego pola przez które iterujemy
//            console.log($(this).val());
            
            var klucz = $(this).attr('name');
            
            klop[klucz] = $(this).val(); 
            
        });
        
//        console.log(klop);
        
        var noweAuto = new Samochod(klop['company'], klop['model'], klop['type'], klop['year'], klop['engine']); // stosujemy klucze
        
        noweAuto.wpis();
        
        $(noweAuto.wpis()).insertAfter('table.carTable tr:last-child'); //insertAfter wklej za ostatnim elementem tr w obrebie carTable
        
        console.log(noweAuto.wpis());
        
        $(this).parents('form').submit();
        

    });
});

