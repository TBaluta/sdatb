<?php
    
    if (isset($_POST) && $_POST !=NULL) {
        echo '<pre>';
        print_r($_POST);
        foreach ($_POST as $key => $val) {
            echo 'klucz: ' . $key . ' wartosc: ' . $val . '<br/>';
        }
        echo '</pre>';
    }
    
    class kursant {
        public $ret;
        public $name;
        
        public function __construct() {
            echo 'utworzono usera';
        }
        
        public function getName() {
            return $this->name;
        }
        
        public function zrobCos() {
            echo 'nie wejdziesz';
        }
    }
    
    $k1 = new kursant('piotr');
    
    echo $k1->getName();
    echo $k1->zrobCos();
?>

<html>
    <head>
        <title>Drugi projekt 13.02</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css.css" />

        <script type="text/javascript" src="jquery-2.2.0.js"></script>
        <script type="text/javascript" src="js.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <form method="POST">

                <div class="row">
                    <label for="company">Wpisz markę:</label>
                    <input id="company" type="text" value="" class="text" placeholder="Marka" name="company"/>
                </div>

                <div class="row">
                    <label for="model">Wpisz model:</label>
                    <input id="model" type="text" value="" class="text" placeholder="Model" name="model"/>
                </div>

                <div class="row">
                    <label for="type">Wpisz typ nadwozia:</label>
                    <input id="type" type="text" value="" class="text" placeholder="Typ nadwozia" name="type"/>
                </div>

                <div class="row">
                    <label for="year">Wpisz rocznik:</label>
                    <input id="year" type="text" value="" class="text" placeholder="Rocznik" name="year"/>
                </div>

                <div class="row">
                    <label for="engine">Wpisz pojemność silnika:</label>
                    <input id="engine" type="text" value="" class="text" placeholder="Pojemność silnika" name="engine"/>
                </div>

                <div class="row">

                    <input type="submit" value="DODAJ" id="button"/>
                </div>
            </form>
            <table class="carTable">
                <thead>
                    <tr>
                        <th>Marka</th>
                        <th>Model</th>
                        <th>Typ nadwozia</th>
                        <th>Rok</th>
                        <th>Pojemność silnika</th>
                    </tr>
                </thead>
            </table>


        </div>

    </body>
</html>
