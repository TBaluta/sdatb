// include things
var gulp = require('gulp');
var connect = require('gulp-connect');
var bower = require('gulp-bower');
var sass = require('gulp-sass');
var rimraf = require('rimraf');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

// paths defined here, to keep the tasks code more cleaner
var paths = {
  libs: ['libs/jquery/dist/jquery.js',
  'libs/modernizr/modernizr.js',
    'libs/angular/angular.min.js',
    'libs/angular-route/angular-route.min.js',
    'libs/angular-bootstrap/ui-bootstrap-tpls.js',
    'libs/gsap/src/minified/TimelineLite.min.js',
    'libs/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js'

  ],
  scripts: ['js/src/*.js', 'js/src/**/*.js'],
  htmls: ['*.html', 'views/*.html'],
  sass: ['styles/sass/*'],
};

// here we go !

// webserver
gulp.task('connect', ['sass'], function() {
  connect.server({
    root: '',
    livereload: true,
    port: 1339,
    fallback: 'index.html'
  });
});

//  clean build folders
gulp.task('clean', function(cb) {
  rimraf('libs', cb);
});

// build libraries
gulp.task('libs', ['bower'], function() {
  return gulp.src(paths.libs)
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest('js/build'));
});

// build user scripts
gulp.task('scripts', [], function() {
  return gulp.src(paths.scripts)
    .pipe(concat('app.js'))
    .pipe(gulp.dest('js/build'))
    .pipe(connect.reload());
});

// bower stuff
gulp.task('bower', ['clean'], function() {
  return bower('libs/');
});

gulp.task('html', function() {
  gulp.src(paths.htmls)
    .pipe(connect.reload());
});

// compile sass
gulp.task('sass', [], function() {
  gulp.src(paths.sass)
    .pipe(sass())
    .pipe(gulp.dest('styles/css/'))
    .pipe(connect.reload());
});

// watch behaviour
gulp.task('watch', function() {

  gulp.watch(paths.less, ['sass']);

  gulp.watch(paths.scripts, ['scripts']);

  gulp.watch(paths.htmls, ['html']);

});


gulp.task('default', ['scripts', 'sass', 'connect', 'watch']);
gulp.task('lib', ['bower', 'libs']);
