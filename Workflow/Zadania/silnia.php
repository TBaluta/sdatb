<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Obliczanie silni iteracyjnie</title>
    </head>
    <body>
        <h3>Obliczanie silni iteracyjnie</h3>

        <?php
        /* Obliczanie silni interacyjnie
          www.algorytm.org */

        function factorial($n) { //iteracyjnie
            if ($n == 1)
                return 1; //$n jest równe jeden, zwracamy jeden
            if ($n == 0)
                return 1; //$n jest równe zero, więc zwracamy jeden
            if ($n > 1) { //jeżeli $n jest większe od 1, to zaczynamy pętle
                $a = 1; //zmienna pomocnicza
                for ($i = 2; $i <= $n; $i++) { //póki zmienna $i domyślnie równa dwa jest mniejsza bądź równa $n, to ją zwiększamy
                    $a *= $i; //wymnażamy $a z $i
                }
                return $a; //zwracamy wynik
            }
        }

        if ($_POST) {
            if (is_numeric($_POST['n'])) {
                echo $_POST['n'] . '! = ' . factorial((int) $_POST['n']) . '<br />' . "\r\n";
            }
        }
        ?>

        <form action="<?= basename($_SERVER['SCRIPT_NAME']); ?>" method="post">
            <input type="text" size="5" name="n" />! = ?<br />
            <input type="submit" name="send" value="Oblicz!" />
        </form>
    </body>
</html>