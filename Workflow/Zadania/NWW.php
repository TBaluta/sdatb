<?php

$x = 13;  //liczby przykładowe, można pobrać wartość np za pomocą formularza
$y = 245;

$a = $x;
$b = $y;

while ($a != $b) {
    if ($a > $b) {
        $a = $a - $b;
    } else {
        $b = $b - $a;
    }
}

$NWD = $a;

$NWW = ($x * $y) / $NWD;

echo('Dla a=' . $x . ' i b=' . $y . ' NWW = ' . $NWW);
