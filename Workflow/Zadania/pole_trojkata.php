<?php
header('Content-Type: text/html; charset=utf-8');

if ($_POST['licz'])
{
   $A['x'] = $_POST['wAx'];
   $A['y'] = $_POST['wAy'];
   $B['x'] = $_POST['wBx'];
   $B['y'] = $_POST['wBy'];
   $C['x'] = $_POST['wCx'];
   $C['y'] = $_POST['wCy'];

   if (bledne_wspolrzedne($A, $B, $C))
   {
       print 'błąd wspolrzednych';
       print '<br><a href="pole_trojkata.php">Powrót do formularza</a>';
   }
   else
   {
/*
** zmienilem nazwy zmiennych, ale Twoj program zle oblicza
** nie znam tego wzoru, wiec sam napisze co mi do glowy przyjdzie
** a przyszlo mi, zeby odjac pola trojatow z kwadratu opisanego na trojkacie o danych wierzcholkach
**      $w = ($A['x']*$B['y'] + $A['y']*$C['x'] + $B['x']*$C['y'] - $C['x']*$B['y'] - $C['y']*$A['x'] - $B['x']*$A['y']) / 2 ;
*/
       $w = pole($A, $B, $C);
       print "Pole trojkata o wprowadzonych współżednych wynosi: $w" ;
       print '<br><a href="pole_trojkata.php">Powrót do formularza</a>';
   }
}
else
{
   print '<form action= "pole_trojkata.php" method=post>';
   print 'Obliczanie pola trójkąta<br>Wpisz wartości x i y wierzchołków do pola formularza<br><br>';
   print 'wierzchołek A ' ;
   print '<input type="text" name="wAx">';
   print '<input type="text" name="wAy"><br>';
   print 'wierzchołek B ' ;
   print '<input type="text" name="wBx">';
   print '<input type="text" name="wBy"><br>';
   print 'wierzchołek C ' ;
   print '<input type="text" name="wCx">';
   print '<input type="text" name="wCy"><br>';
   print '<input type="submit" name="licz" value="Licz">';
   print '</form>';
}

function bledne_wspolrzedne($w1, $w2, $w3)
{
   //sprawdzenie czy wpolrzedne zadnego z wierzcholkow nie sa takie same
   if ($w1 == $w2)
       return true;
   elseif ($w1 == $w3)
       return true;
   elseif ($w2 == $w3)
       return true;
   //sprawdzenie czy wierzcholki nie leza w jednej lini
   elseif (linia($w1, $w2, $w3))
       return true;
   return false;
}

function linia($w1, $w2, $w3)
{
   if ($w1['x'] == $w2['x'] && $w1['x'] == $w3['x'])
       return true;    //linia pionowa
   elseif ($w1['y'] == $w2['y'] && $w1['y'] == $w3['y'])
       return true;    //linia pozioma
   else
   {
       $x = $w1['x'] - $w2['x'];
       $y = $w1['y'] - $w2['y'];

       $a = $w3['x'] / $x;
       $b = $w3['y'] / $y;
       if ($x / $y == $a / $b) //nie jest to linia pozioma, wiec mozna bez problemu dzielic bez obawy, ze wyjdzie zero ;]
           return true;    //linia ukosna
   }
   return false;   //nie jest to linia
}

function pole($w1, $w2, $w3)
{
   $lewo = min($w1['x'], $w2['x'], $w3['x']);
   $prawo = max($w1['x'], $w2['x'], $w3['x']);
   $dol = min($w1['y'], $w2['y'], $w3['y']);
   $gora = max($w1['y'], $w2['y'], $w3['y']);

   $pole_kwadratu = abs(($prawo - $lewo) * ($gora - $dol));
   $pole_t1 = pole_trojkata($w1, $w2);
   $pole_t2 = pole_trojkata($w1, $w3);
   $pole_t3 = pole_trojkata($w2, $w3);

   return ($pole_kwadratu - $pole_t1 - $pole_t2 - $pole_t3);
}

function pole_trojkata($w1, $w2)
{
   $x = abs($w1['x'] - $w2['x']);
   $y = abs($w1['y'] - $w2['y']);

   return ($x * $y / 2);
}
