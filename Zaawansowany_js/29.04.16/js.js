function Kik() {
    var container = $('.row'),
            element = $('.col-md-4'),
            znak = "x",
            tablica = ['012', '345', '678', '036', '147', '258', '048', '246'],
            winner = false;
    element.on('click', function () {
        if ($(this).html() !== '') {
            return;
        }
        if (znak === "x") {
            znak = "o";
        } else {
            znak = "x";
        }
        if ($(this).text() === "")
            $(this).html(znak);
        win(znak);
        if (winner === true) {
            element.off('click');
        }
    });

    function createId() {
        for (var i = 0; i < 9; i++) {
            $('.col-md-4:eq(' + i + ')').attr('data-nr', i);
        }
    }
    createId();

    function win(param) {
        var e;
        for (var i = 0; i < tablica.length; i++) {
            e = convertArray(tablica[i].split(''));

            if ($('.col-md-4:eq(' + [e[0]] + ')').html() === param &&
                    $('.col-md-4:eq(' + [e[1]] + ')').html() === param &&
                    $('.col-md-4:eq(' + [e[2]] + ')').html() === param) {

                for (i = 0; i < 3; i++) {
                    $('.col-md-4:eq(' + [e[i]] + ')').css("color", "green");
                }

                alert('WIN');
                winner = true;
                return;
            }
        }
    }

    function convertArray(param2) {
        for (var i in param2) {
            param2[i] = parseInt(param2[i]);
        }
        return param2;
    }
}

Kik();


