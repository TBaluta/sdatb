//function stworzObiekt()
//{
//    function f1()
//    {
//        console.log('wywolano mnie #1');
//        return 2;
//    }
//
//    function f2()
//    {
//        console.log('wywolano mnie #2');
//        return f1() + 3;
//    }
//
//    function f3()
//    {
//        console.log('rabarbar');
//    }
//
//    return {
//        func1: f1,
//        func2: f2,
//        rabarbar: f3
//    };
//}
//
//var MojObiekt = stworzObiekt();
//
//console.log(MojObiekt.func2());
//
//MojObiekt.rabarbar();

//////////////////////////////// ZADANIE 1 //////////////////////////////////////////
http://pfig.ansta.pl/baluta/mojprojekt/zaawansowany_js/27.04.16/index.html

function stworzTrojkat()
{
    var bok1 = parseInt(prompt("Podaj długość pierwszego boku"));
    var bok2 = parseInt(prompt("Podaj długość drugiego boku"));
    var bok3 = parseInt(prompt("Podaj długość trzeciego boku"));

    function liczBoki()
    {
        var max = Math.max(bok1, bok2, bok3);
        var suma = bok1 + bok2 + bok3;
        return((suma - max) > max) ? true : false;
    }
    return {
        a : bok1,
        b : bok2,
        c : bok3,
        sprawdzBoki : liczBoki
    };
}

var dziwnyTrojkat = stworzTrojkat();

console.log(dziwnyTrojkat.sprawdzBoki());

//////////////////////////////// ZADANIE 2 //////////////////////////////////////////

function superLiczby()
{
    var a = parseInt(prompt('Podaj pierwszą liczbę'));
    var b = parseInt(prompt('Podaj drugą liczbę'));
    var start = a;
    var end = b;
    var tablica = [];

    function liczParzyste()
    {

        if (a > b) {
            start = b;
            end = a;
        }
        for (var i = start; i <= end; i++) {
            if (i % 2 === 0) {
                tablica.push(i);
            }
        }
        for (i = end; i >= start; i--) {
            if (i % 2 !== 0) {
                tablica.push(i);
            }
        }
        console.log(tablica.join(','));
    }
    return {
        digit : liczParzyste
    };
}
var liczby = superLiczby();
liczby.digit();

//////////////////////////////// ZADANIE 3 //////////////////////////////////////////

function superPiramidaZrabarbaru()
{
    var n = parseInt(prompt('Podaj liczbę rabarbarowych kondygnacji'));
    var sumaRabarbaru = 0;
    var tablica = [];

    function ileTrzaRabarbaru() {
        for (i = 1; i <= n; i++) {
            sumaRabarbaru += i;
        }
        console.log(sumaRabarbaru);
    }

    function rysujRabarbarowaChoinke() {

            for (i = 1; i <= n; i++) {
                for (j=0; j<=((n-i)|0)/2; j++) {
                    tablica.push(' ');
                }
                for(k=1;k<=i; k++) {
                    tablica.push('*');
                }
                tablica.push("\r\n");
            }
        
        console.log(tablica.join(''));
    }

    return {
        vegetable: ileTrzaRabarbaru,
        draw : rysujRabarbarowaChoinke
    };
}

var kompost = superPiramidaZrabarbaru();
kompost.vegetable();
kompost.draw();

//////////////////////////////// ZADANIE //////////////////////////////////////////

///////////////////////// 28.04.16 ///////////////////////////////////////////////

