function adress(rangeEnd, streetName) {
    rangeEnd = (rangeEnd % 2 === 0) ? rangeEnd-- : rangeEnd; // zapis skrócony ifa
    for (var block = 1; block <= rangeEnd; block += 2) {
         
        for (i=1; i<=12; i++) {
            if (i<=6) {
                var currAddr = streetName + ' ' + block;
                currAddr+='/A/'+i;
            } else {
                var currAddr = streetName + ' ' + block;
                currAddr+='/B/'+(i-6);
            }
            console.log(currAddr);
        }
    }
}
adress(13, 'Algorytmiczna');

//////////////////////////////////////////////////////////////////////////////////////

function palindrom(element) {
    if(typeof(element)!=='object') {
        var toString = (String(element)).toLowerCase().split(' ').join('').split('');
        console.log(toString);
        var reverseString = (String(element)).toLowerCase().split(' ').join('').split('').reverse();
        console.log(reverseString);
        
        if(JSON.stringify(toString) === JSON.stringify(reverseString)) {
            console.log('Jest palindromem');
        } else {
            console.log('Nie jest palindromem');
        }
    } else {
        if(JSON.stringify(element) === JSON.stringify(element)) {
            console.log('Jest palindromem');
        }
    }
}
palindrom('Elf układał kufle');

////////////////////////////////////////////////////////////////////////////////////////

function szyfrCezara(haslo, przesuniecie) {
    var wyjscie = '';
    var hasloString = String(haslo);
    for (var i = 0; i < hasloString.length; i++) {
        var litera = hasloString.charCodeAt(i);
//        console.log(litera);
        litera += przesuniecie;
        var literaZaszyfrowana = String.fromCharCode(litera);
        wyjscie += literaZaszyfrowana;

    }
    return wyjscie;
}
console.log(szyfrCezara('dupa', 5));

////////////////////////////////////////////////////////////////////////////////////

function ROT13(haslo) {
    var alfabet = [];
    var toString = haslo.toString().split('');
    for (var i = 65; i <= 122; i++) {
        if (!(i < 96 && i > 91)) {
            alfabet.push(String.fromCharCode(i));
        }
    }

    for (var j = 0; j < toString.length; j++) {
        var k = (alfabet.indexOf(toString[j]))+13;
        if (k >= alfabet.length) {
            k -=alfabet.length;
        }
        var literka = alfabet[k];
        toString[j]=literka;
        
    }
    console.log(alfabet);
    return toString.join('');
}

console.log(ROT13('dupa'));
console.log(ROT13('qHCn'));

//////////////////////////////////////////////////////////////////////////////////

function notacja(wyrazenie) {
    var wyrazenieTablica = wyrazenie.split(' ');
    console.log(wyrazenieTablica);
    var wynik = 0;
    var znaki = ['+', '-', '*', '/'];

    for (var i = 0; i < wyrazenieTablica.length; i++) {
        var element = wyrazenieTablica.pop();
        if (znaki.indexOf(element) === -1) {
            console.log('indexOf');
            element = parseInt(element);
            wyrazenieTablica.push(element);
        } else {
            var liczba1 = parseInt(wyrazenieTablica.pop());
            var liczba2 = parseInt(wyrazenieTablica.pop());
            switch (wyrazenieTablica[i]) {
                case '+':
                    wynik = liczba1 + liczba2;
                    break;
                case '-':
                    wynik = liczba1 - liczba2;
                    break;
                case '*':
                    wynik = liczba1 * liczba2;
                    break;
                case '/':
                    wynik = liczba1 / liczba2;
                    break;
            }
        }
        console.log(wyrazenieTablica);
    }
}
notacja('2 7 + 3 / 14 3 - 4 * + 2 /');

//////////////////////////////////////////////////////////////////////////////////////////

function liczbaNaturalna(odZakres, doZakres) 
{
    var wylosowanaLiczba = Math.floor(odZakres + Math.random() * (doZakres - odZakres));
    console.log(wylosowanaLiczba);
    var ileRazy = parseInt(prompt('Ile razy byś chciał zgadywać koleżko?'));
    console.log(ileRazy);
    var zgadl = false;

    for (i = 0; i < ileRazy; i++) {
        var aktualnaLiczba = parseInt(prompt('Próba ' + (i + 1) + '. Podaj liczbę'));

        if (aktualnaLiczba === wylosowanaLiczba) {
            zgdal = true;
            alert('Zgadłeś koleżko! Poszukiwaną liczbą jest: ' + aktualnaLiczba);
            break;
        }

        if (i === ileRazy - 1) {
            alert('Dałeś dupy! Wylosowana liczba to: ' + wylosowanaLiczba);
        }
    }
}
liczbaNaturalna(20, 50);

/////////////////////////////////////////////////////////////////////////////////////

function liczbaNaturalna(odZakres, doZakres) 
{
    var wylosowanaLiczba = Math.floor(odZakres + Math.random() * (doZakres - odZakres));
    console.log(wylosowanaLiczba);
    alert('Liczba została wylosowana z przedziału od ' + odZakres + ' do ' + doZakres + ' Zgaduj!');
    var zgadl = false;
    var i = 1;

    while (zgadl === false) {
        var liczba = parseInt(prompt('Próba ' + i + '. Podaj liczbę'));

        if (liczba === -1) {
            alert('Dałeś dupy! Zużyłeś ' + i + ' prób. Wylosowana liczba to: ' + wylosowanaLiczba);
            break;
        }

        if (liczba === wylosowanaLiczba) {
            zgdal = true;
            alert('Zgadłeś koleżko! Poszukiwaną liczbą jest: ' + liczba);
            break;
        }
        i++;
    }
}
liczbaNaturalna(20, 50);

///////////////////////////////////////////////////////////////////////////////////////////////

function wisielec () {
    var slownik = ['dupa', 'cycki', 'ciastko', 'stringi', 'krzesło', 'rodzynki'];
    var index = Math.random() * slownik.length |0;
//    console.log(index);
    
    var slowoDoZgadniecia = slownik[index].split('');
    console.log(slowoDoZgadniecia);
    var slowoZakodowane = '';
    
    for (var i = 0; i < slowoDoZgadniecia.length; i++) {
        slowoZakodowane += '_';
    }
    
    var haslo = slowoZakodowane.split(''),
    j = 1;
    
    while (JSON.stringify(haslo) !== JSON.stringify(slowoDoZgadniecia)) { // zamiast jsona mozna uzyc string zeby porownac tablice
        
        var litera = prompt('Podaj literę');
        
        if (litera === -1) {
            alert('Przerwałeś grę');
            break;
        } else if (litera.length > 1) {
            alert('Wpisałeś za dużo znaków');
            continue;
        }
        
        for (var i=0; i<slowoDoZgadniecia.length; i++ ) {
            if (slowoDoZgadniecia[i] === litera) {
                haslo[i]=slowoDoZgadniecia[i];
            }
        }

//        lub to samo forEachem
//        slowoDoZgadniecia.forEach(function(el,i) {
//            console.log(el,i);
//            if(slowoDoZgadniecia[i] == litera) {
//                haslo[i] = slowoDoZgadniecia[i];
//            }
//        }

        if (JSON.stringify(haslo) === JSON.stringify(slowoDoZgadniecia)) {
            alert('Wygrałeś w ' + j + ' próbie');
        }
        console.log(haslo);
        j++;
    }
    
}
wisielec();

////////Obiekty////////////////////////////////////////////////////////////////////////////// 

function myObject (arg) 
{
    var warzywa = {};
    warzywa.rabarbar = arg;
    warzywa.kalarepa = 5;
    warzywa.szczaw = function() 
    {
        console.log(this.rabarbar);
    };
    
    return warzywa;
}
var x = myObject(7);
x.szczaw();
console.log(x.kalarepa); // daje 5
x.kalarepa = 3.5;
console.log(x.kalarepa); // daje 3.5

////////Prototypy////////////////////////////////////////////////////////////////////////////// 

//function Owoce() {}
//Owoce.prototype.jakasFunkcja = function()
//{
//    alert('Ale super i w ogóle');
//};
//var OwoceWarzywa = new Owoce();
//OwoceWarzywa.jakasFunkcja();

////////////////////////////Inny przykład//////////////////////////////////

//function Owoce() {}
//Owoce.prototype.sentence = 'przykładowy tekst';
//
//var OwoceWarzywa = new Owoce();
//OwoceWarzywa.jakasFunkcja();

////////////////////////////Inny przykład//////////////////////////////////

//function Obiekt() {}
//Obiekt.prototype.kolor = 'czerwony';
//
//var inst1 = new Obiekt();
//var inst2 = new Obiekt();  
//
//inst2.kolor = 'zielony';
//console.log(inst1.kolor); // czerwony
//console.log(inst2.kolor); // zielony
//
//var inst3 = new Obiekt();
//console.log(inst3.kolor); // czerwony


/////////////////////////////////    ZADANIA     ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

// Zadanie 1 ///////////////////////////////////////////////////////////////////////

function Ksztalt() {};
Ksztalt.prototype.typ = 2;
Ksztalt.prototype.getTyp = function ()
{
    console.log(this.typ);
};
function Trojkat(a, b, c)
{
    if (a) {
        this.a = a;
    }
    if (b) {
        this.b = b;
    }
    if (c) {
        this.c = c;
    }
};

Trojkat.prototype = new Ksztalt();
Trojkat.prototype.a = null;
Trojkat.prototype.b = null;
Trojkat.prototype.c = null;
Trojkat.prototype.getBok = function (bok)
{
    var n = null;
    switch (bok) {
        case 'a':
            n = this.a;
            break;
        case 'b':
            n = this.b;
            break;
        case 'c':
            n = this.c;
            break;
    }
    return n;
};
var x = new Trojkat(1, 2, 3);
//console.log(x.typ);

var y = new Trojkat(5, 20);
console.log(y.getBok('a'));
console.log(y.getBok('b'));
console.log(y.getBok('c'));

var z = new Trojkat(5, 30, 40);

// Zadanie 2 ///////////////////////////////////////////////////////////////////////

function Imie(Im, Naz) {
    if (Im) {
        this.Im = Im;
    }
    if (Naz) {
        this.Naz = Naz;
    }
};

Imie.prototype.Im = null;
Imie.prototype.Naz = null;
Imie.prototype.rzotkief = function () 
{
    var n = parseInt(prompt('Ile razy byś chciał, żeby się wyświetliło Twoje imię i nazwisko?'));
    for (var i =1; i<=n; i++) {
        console.log(this.Im + ' ' + this.Naz);
    }
}
var xyz = new Imie('Goran', 'Jagodnik');
xyz.rzotkief();

// Zadanie 3 ///////////////////////////////////////////////////////////////////////

function Liczby() {
    this.start = null;
    this.end = null;
    this.setRange = function () {
        this.start = parseInt(prompt('Start'));
        this.end = parseInt(prompt('Koniec'));

        if (this.start > this.end) {
            var tmp = this.start;
            this.start = this.end;
            this.end = tmp;
        }
    }
    this.printEvenNumbers = function () {
        var i = this.start;
        while (i <= this.end) {
            if (i % 2 === 0) {
                console.log(i);
            }
            i++;
        }
    }
}

var gruszka = new Liczby();
gruszka.setRange();
gruszka.printEvenNumbers();

// Zadanie 4 ///////////////////////////////////////////////////////////////////////

function Cos(a,b) {
    this.liczba1 = a;
    this.liczba2 = b;
}

Cos.prototype.suma = function() {
    console.log(this.liczba1 + this.liczba2);
};
Cos.prototype.odej = function() {
    console.log(this.liczba1 - this.liczba2);
};
Cos.prototype.mnoz = function() {
    console.log(this.liczba1 * this.liczba2);
};
Cos.prototype.dziel = function() {
    if(this.liczba1 === 0) {
        alert('No dzie przez 0!?');
    }
    console.log(this.liczba1 / this.liczba2);
};
Cos.prototype.all = function () {
    this.suma();
    this.odej();
    this.mnoz();
    this.dziel();
    console.log('dupa');
};

var x = new Cos(2,5);
x.all();
console.log('dupa');
