<?php

header('Content-Type: application/json');

class validator {

    public function sprawdzImieNazwisko($txt) {
        if (strlen($txt) < 3) {
            return false;
        } else {
            return true;
        }
    }

    public function sprawdzEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    public function sprawdzWszystkoZPosta() {
        $odp = array();
        $odp['imie'] = $this->sprawdzImieNazwisko($_POST['imie']);
        $odp['nazwisko'] = $this->sprawdzImieNazwisko($_POST['nazwisko']);
        $odp['email'] = $this->sprawdzEmail($_POST['email']);
        $odp['wiek'] = (int) $_POST['wiek'] < 18 ? false : true;
        $odp['zgoda'] = $_POST['zgoda'] ? true : false; // skrócona instrukcja warunkowa

        json_encode($odp);
    }

}

$validator = new validator();
$validator->sprawdzWszystkoZPosta();
?>
