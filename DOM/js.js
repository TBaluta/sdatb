var books = [
    {
        "title": "Tytul 1",
        "author": "Author 1",
        "isbn": "isbn 1",
        "params": {
            "cena": {
                "netto": 100,
                "vat": 8,
                "brutto": 108
            },
            "count": 1
        }
    },
    {
        "title": "Tytul 2",
        "author": "Author 2",
        "isbn": "isbn 2",
        "params": {
            "cena": {
                "netto": 200,
                "vat": 8,
                "brutto": 216
            },
            "count": 1
        }
    }
];


